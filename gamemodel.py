from math import sin,cos,radians
from random import random
from typing import NamedTuple, SupportsFloat

Aim = NamedTuple('Aim', [('angle', SupportsFloat), ('velocity', SupportsFloat)])


class Game:
    """ This is the model of the game"""
    current_player_number = 0  # Player 0 always starts the first round!

    def __init__(self, cannon_size: SupportsFloat, ball_size: SupportsFloat):
        """ Create a game with a given size of cannon (length of sides) and projectiles (radius) """
        self.cannon_size = cannon_size
        self.ball_size = ball_size
        self.current_wind = self.random_wind()
        self.players = [
            Player(game=self, color='blue', position=-90),
            Player(game=self, color='red', position=90),
        ]

    def getPlayers(self) -> list:
        """ A list containing both players """
        return self.players

    def getCannonSize(self) -> SupportsFloat:
        """ The height/width of the cannon """
        return self.cannon_size

    def getBallSize(self) -> SupportsFloat:
        """ The radius of cannon balls """
        return self.ball_size

    def getCurrentPlayer(self):
        """ The current player, i.e. the player whose turn it is """
        return self.players[self.current_player_number]

    def getOtherPlayer(self):
        """ The opponent of the current player """
        # Since current_player_number will always be either 0 or 1,
        # we can exploit the mapping of False/True to easily flip
        # between them by using `not`.
        return self.players[not self.current_player_number]

    def getCurrentPlayerNumber(self) -> int:
        """ The number (0 or 1) of the current player. This should be the position of the current player in getPlayers(). """
        return self.current_player_number

    def nextPlayer(self) -> None:
        """ Switch active player """
        # See comment about logic used in self.getOtherPlayer()
        self.current_player_number = int(not self.current_player_number)

    @staticmethod
    def random_wind() -> float:
        """Return random float between -10.0 and 10.0."""
        return 10-(random()*20)

    def setCurrentWind(self, wind: SupportsFloat) -> None:
        """ Set the current wind speed, only used for testing """
        self.current_wind = wind
    
    def getCurrentWind(self) -> SupportsFloat:
        return self.current_wind

    def newRound(self) -> None:
        """ Start a new round with a random wind value (-10 to +10) """
        self.current_wind = self.random_wind()


class Player:
    """ Models a player """
    aim = Aim(0, 0)
    score: int = 0

    def __init__(self, game: Game, color: str, position: SupportsFloat):
        self.game = game
        self.color = color
        self.position = position

    def fire(self, angle: SupportsFloat, velocity: SupportsFloat):
        """ Create and return a projectile starting at the centre of this players cannon. Replaces any previous projectile for this player. """
        self.aim = Aim(angle, velocity)
        if self.position > 0:
            # “Reverse” angle if shooting opposite direction
            angle = 180 - angle
        # The projectile should start in the middle of the cannon of the firing player
        return Projectile(angle=angle, velocity=velocity,
                          wind=self.game.getCurrentWind(),
                          xPos=self.position, yPos=self.game.cannon_size/2,
                          xLower=-110, xUpper=110)

    def projectileDistance(self, proj) -> SupportsFloat:
        """ Gives the x-distance from this players cannon to a projectile. If the cannon and the projectile touch (assuming the projectile is on the ground and factoring in both cannon and projectile size) this method should return 0"""
        # ball_size is given in radius, so it is already halved
        allowed_variation = self.game.ball_size + self.game.cannon_size/2
        center_distance = proj.getX() - self.getX()
        edge_distance = abs(center_distance) - allowed_variation

        if abs(center_distance) <= allowed_variation:
            return 0
        if center_distance < 0:
            return -edge_distance
        return edge_distance

    def getScore(self) -> int:
        """ The current score of this player """
        return self.score

    def increaseScore(self) -> None:
        """ Increase the score of this player by 1."""
        self.score += 1

    def getColor(self) -> str:
        """ Returns the color of this player (a string)"""
        return self.color

    def getX(self) -> SupportsFloat:
        """ The x-position of the centre of this players cannon """
        return self.position

    def getAim(self) -> Aim:
        """ The angle and velocity of the last projectile this player fired, initially (45, 40) """
        return self.aim


class Projectile:
    """Models a projectile (a cannonball, but could be used more generally)

        Constructor parameters:
        angle and velocity: the initial angle and velocity of the projectile 
            angle 0 means straight east (positive x-direction) and 90 straight up
        wind: The wind speed value affecting this projectile
        xPos and yPos: The initial position of this projectile
        xLower and xUpper: The lowest and highest x-positions allowed
    """
    def __init__(self, angle, velocity, wind, xPos, yPos, xLower, xUpper):
        self.yPos = yPos
        self.xPos = xPos
        self.xLower = xLower
        self.xUpper = xUpper
        theta = radians(angle)
        self.xvel = velocity*cos(theta)
        self.yvel = velocity*sin(theta)
        self.wind = wind

    def update(self, time):
        """
            Advance time by a given number of seconds
            (typically, time is less than a second,
             for large values the projectile may move erratically)
        """
        # Compute new velocity based on acceleration from gravity/wind
        yvel1 = self.yvel - 9.8*time
        xvel1 = self.xvel + self.wind*time
        
        # Move based on the average velocity in the time period 
        self.xPos = self.xPos + time * (self.xvel + xvel1) / 2.0
        self.yPos = self.yPos + time * (self.yvel + yvel1) / 2.0
        
        # make sure yPos >= 0
        self.yPos = max(self.yPos, 0)
        
        # Make sure xLower <= xPos <= mUpper   
        self.xPos = max(self.xPos, self.xLower)
        self.xPos = min(self.xPos, self.xUpper)
        
        # Update velocities
        self.yvel = yvel1
        self.xvel = xvel1
        
    def isMoving(self):
        """ A projectile is moving as long as it has not hit the ground or moved outside the xLower and xUpper limits """
        return 0 < self.getY() and self.xLower < self.getX() < self.xUpper

    def getX(self):
        return self.xPos

    def getY(self):
        """ The current y-position (height) of the projectile". Should never be below 0. """
        return self.yPos
