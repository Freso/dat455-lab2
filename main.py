from typing import Union

from gamemodel import *
from gamegraphics import *


def graphic_input(player: Player, game: Game) -> Union[Aim, str]:
    """Deal with input from the user."""
    old_aim = player.aim
    controls = InputDialog(angle=old_aim.angle, vel=old_aim.velocity, wind=game.current_wind)
    while True:
        action = controls.interact()
        if action == 'Quit':
            return action
        elif action == 'Fire!':
            aim = Aim(*controls.getValues())
            controls.close()
            return aim


def finish_shot(game: GraphicGame, projectile: GraphicProjectile) -> None:
    """Calculate new values based on shot and clean up."""
    player = game.getCurrentPlayer()
    opponent = game.getOtherPlayer()

    distance = opponent.projectileDistance(projectile)
    if distance == 0:
        # Opponent’s canon was hit! Time for a new round.
        player.increaseScore()
        game.newRound()

    # Switch turns
    game.nextPlayer()


# Here is a nice little method you get for free
# It fires a shot for the current player and animates it until it stops
def graphic_fire(game, angle, vel):
    player = game.getCurrentPlayer()
    # create a shot and track until it hits ground or leaves window
    gproj = player.fire(angle, vel)
    while gproj.isMoving():
        gproj.update(1/50)
        update(50)
    return gproj


def graphic_play():
    """Main game loop."""
    game = GraphicGame(Game(10, 3))
    while True:
        aim = graphic_input(game.getCurrentPlayer().player, game.game)
        if aim == 'Quit':
            break
        proj = graphic_fire(game, aim.angle, aim.velocity)
        finish_shot(game, proj)
    print('Final score is…')
    p1pts = game.players[0].getScore()
    p2pts = game.players[1].getScore()
    print('Player 1: {} points'.format(p1pts))
    print('Player 2: {} points'.format(p2pts))
    if p1pts != p2pts:
        print('The winner is Player {}!!!'.format('1' if (p1pts > p2pts) else '2'))
    else:
        print('It’s a tie!!')


# Run the game with graphical interface
if __name__ == '__main__':
    graphic_play()
