#------------------------------------------------------
#This module contains all graphics-classes for the game
#Most classes are wrappers around model classes, e.g. 
#  * GraphicGame is a wrappoer around Game
#  * GraphicPlayer is a wrapper around Player
#  * GraphicProjectile is a wrapper around Projectile
#In addition there are two UI-classes that have no 
#counterparts in the model:
#  * Button
#  * InputDialog
#------------------------------------------------------

from typing import SupportsFloat

# This is the only place where graphics should be imported!
from graphics import *


class GraphicGame:

    def __init__(self, game) -> None:
        self.game = game
        self.window = GraphWin('Cannon game', 640, 480, autoflush=False)
        self.window.setCoords(-110, -10, 110, 155)
        Line(Point(-110, 0), Point(110, 0)).draw(self.window)
        self.players = [GraphicPlayer(game.players[0], self),
                        GraphicPlayer(game.players[1], self)]

    def getPlayers(self) -> list:
        return self.players

    def getCurrentPlayer(self):
        return self.players[self.game.current_player_number]

    def getOtherPlayer(self):
        return self.players[not self.game.current_player_number]

    def getCurrentPlayerNumber(self) -> int:
        return self.game.getCurrentPlayerNumber()

    def getCannonSize(self) -> SupportsFloat:
        return self.game.getCannonSize()

    def getBallSize(self) -> SupportsFloat:
        return self.game.getBallSize()

    def getCurrentWind(self) -> float:
        return self.game.getCurrentWind()

    def setCurrentWind(self, wind: SupportsFloat) -> None:
        self.game.current_wind = wind

    def nextPlayer(self) -> None:
        self.game.nextPlayer()

    def newRound(self) -> None:
        self.game.newRound()

    def getWindow(self) -> GraphWin:
        return self.window


class GraphicPlayer:
    g_proj = None

    def __init__(self, player, g_game: GraphicGame) -> None:
        self.player = player
        self.g_game = g_game
        self.g_score = Text(Point(player.position, -5), '')
        self._update_score_text()
        self.g_cannon = self._make_cannon(g_game.window, position=player.position,
                                          cannon_size=g_game.game.cannon_size, color=player.color)

    @staticmethod
    def _make_cannon(window: GraphWin, position: SupportsFloat,
                     cannon_size: SupportsFloat, color: str) -> Rectangle:
        cannon = Rectangle(
            Point(position-(cannon_size/2), cannon_size),
            Point(position+(cannon_size/2), 0))
        cannon.setFill(color)
        cannon.draw(window)
        return cannon

    def _update_score_text(self) -> None:
        self.g_score.setText('Score: {}'.format(self.player.score))
        self.g_score.undraw()
        self.g_score.draw(self.g_game.window)

    def fire(self, angle, vel):
        # Fire the cannon of the underlying player object
        proj = self.player.fire(angle, vel)
        if 'undraw' in dir(self.g_proj):
            self.g_proj.undraw()
        g_proj = GraphicProjectile(proj, self.g_game.window, self.player)
        self.g_proj = g_proj
        return g_proj
    
    def getAim(self):
        return self.player.getAim()
        
    def getColor(self):
        return self.player.getColor()

    def getX(self):
        return self.player.getX()

    def getScore(self):
        return self.player.getScore()

    def projectileDistance(self, proj):
        return self.player.projectileDistance(proj)
        
    def increaseScore(self):
        self.player.increaseScore()
        self._update_score_text()


""" A graphic wrapper around the Projectile class (adapted from ShotTracker in book)"""
class GraphicProjectile:

    def __init__(self, projectile, window: GraphWin, player) -> None:
        self.proj = projectile
        self.window = window
        self.player = player
        self.circle = Circle(Point(self.proj.getX(), self.proj.getY()),
                             self.player.game.ball_size)
        self.circle.setFill(self.player.color)
        self._draw_circle()

    def _draw_circle(self) -> None:
        dx = self.proj.getX() - self.circle.getCenter().getX()
        dy = self.proj.getY() - self.circle.getCenter().getY()
        self.circle.move(dx, dy)
        self.undraw()
        self.circle.draw(self.window)

    def update(self, dt):
        # update the projectile
        self.proj.update(dt)
        self._draw_circle()
        
    def getX(self):
        return self.proj.getX()

    def getY(self):
        return self.proj.getY()

    def isMoving(self):
        return self.proj.isMoving()

    def undraw(self) -> None:
        self.circle.undraw()


""" A somewhat specific input dialog class (adapted from the book) """
class InputDialog:
    """ Takes the initial angle and velocity values, and the current wind value """
    def __init__ (self, angle, vel, wind):
        self.win = win = GraphWin("Fire", 200, 300)
        win.setCoords(0,4.5,4,.5)
        Text(Point(1,2), "Velocity").draw(win)
        self.vel = Entry(Point(3,2), 5).draw(win)
        self.vel.setText(str(vel))

        Text(Point(1, 1), "Angle").draw(win)
        self.angle = Entry(Point(3, 1), 5).draw(win)
        self.angle.setText(str(angle))

        Text(Point(1,3), "Wind").draw(win)
        self.height = Text(Point(3,3), 5).draw(win)
        self.height.setText("{0:.2f}".format(wind))
        
        self.fire = Button(win, Point(1,4), 1.25, .5, "Fire!")
        self.fire.activate()
        self.quit = Button(win, Point(3,4), 1.25, .5, "Quit")
        self.quit.activate()

    """ Runs a loop until the user presses either the quit or fire button """
    def interact(self):
        while True:
            pt = self.win.getMouse()
            if self.quit.clicked(pt):
                return "Quit"
            if self.fire.clicked(pt):
                return "Fire!"

    """ Returns the current values of (angle, velocity) as entered by the user"""
    def getValues(self):
        a = float(self.angle.getText())
        v = float(self.vel.getText())
        return a,v

    def close(self):
        self.win.close()



""" A general button class (from the book) """
class Button:

    """A button is a labeled rectangle in a window.
    It is activated or deactivated with the activate()
    and deactivate() methods. The clicked(p) method
    returns true if the button is active and p is inside it."""

    def __init__(self, win, center, width, height, label):
        """ Creates a rectangular button, eg:
        qb = Button(myWin, Point(30,25), 20, 10, 'Quit') """ 

        w,h = width/2.0, height/2.0
        x,y = center.getX(), center.getY()
        self.xmax, self.xmin = x+w, x-w
        self.ymax, self.ymin = y+h, y-h
        p1 = Point(self.xmin, self.ymin)
        p2 = Point(self.xmax, self.ymax)
        self.rect = Rectangle(p1,p2)
        self.rect.setFill('lightgray')
        self.rect.draw(win)
        self.label = Text(center, label)
        self.label.draw(win)
        self.deactivate()

    def clicked(self, p):
        "RETURNS true if button active and p is inside"
        return self.active and \
               self.xmin <= p.getX() <= self.xmax and \
               self.ymin <= p.getY() <= self.ymax

    def getLabel(self):
        "RETURNS the label string of this button."
        return self.label.getText()

    def activate(self):
        "Sets this button to 'active'."
        self.label.setFill('black')
        self.rect.setWidth(2)
        self.active = 1

    def deactivate(self):
        "Sets this button to 'inactive'."
        self.label.setFill('darkgrey')
        self.rect.setWidth(1)
        self.active = 0